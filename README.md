Script para generación de imágenes de firmware para **LaOtraRed La Paz**.

**LICENCIA**: GPL v3

    Uso:
     ./chef.sh modelo_router bloque_IP/CIDR
     ./chef.sh [-l] [-h][--help]
     
     Donde:
       modelo_router    El codigo segun el ImageBuilder , del enrutador.
       bloque_IP/CIDR  El bloque de direcciones IPv4 y el /CIDR asignado al router.
       -l               Lista los codigos segun los modelos de routers.
       -h, --help        Muestra esta ayuda.
     
     IMPORTANTE:
     El script requiere leer archivos PLANTILLA de configuraciones y estos deben estar ubicados en una carpeta en el directorio del script y tener el nombre 'configuraciones_chef'. 
     Por cada modelo de router debe haber una carpeta nombrada con el CODIGO del MODELO del ROUTER y que contenga los archivos de configuraciones.
     El script deberia estar ubicado en el directorio justo antes del  ImageBuilder que se usara por defecto, tambien funcionaria un enlace simbolico.
	 
	 Los modelos soportados estan en los archivos `modelos_2.4Ghz.txt' y `modelos_dualBand.txt'
     
     Autor(es):   
                * Rodrigo Garcia <strysg@riseup.net
	 Repositorio: https://git.laotrared.net/LaOtraRed-dev/generador-firmwares-script

#### configuracion inicial ####

Se requiere el bash, ipcalc, sed, awk, grep -e , make, zip y el **ImageGenerator** de un proyecto mantenido como lede.

1. Descargar cualquiera de los siguientes ImageBuilder:

* https://downloads.lede-project.org/snapshots/targets/ar71xx/generic/lede-imagebuilder-ar71xx-generic.Linux-x86_64.tar.xz
* http://lede.rmgss.net/bin/targets/ar71xx/generic/lede-imagebuilder-ar71xx-generic.Linux-x86_64.tar.xz (repo con el que se probo)

2. Descomprimir el ImageGenerator o imageBuilder.
3. Clonar este repositorio `git clone https://git.laotrared.net/LaOtraRed-dev/generador-firmwares-script`
4. cd generador-firmwares-script
5. Crear un enlace simbolico para que el script sepa desde donde invocar al **imageBuilder**:

        ln --symbolic /ruta/absoluta/hacia/donde/esta/el/imageBuilder/descomprimido $PWD/lede-imagebuilder-ar71xx-generic.Linux-x86_64

6. Ejecutar el script.

#### Ejemplos de ejecucion ####

	# creara una imagen para el tl-mr3020-v1 y la salida del script indica donde quedo la imagen generada.
    ./chef.sh tl-mr3020-v1 10.64.3.64/27

    # similar al anterior pero para el tl-wdr3600-v1 con distinto bloque IPv4
	./chef.sh tl-mr3020-v1 10.64.14.0/26

#### Archivos de PLANTILLA ####

Deben estar dentro el directorio `configuraciones_chef`, por ejemplo:

    configuraciones_chef/tl-mr3020-v1

Que sería el directorio para el router cuyo código es: tl-mr3020-v1.

A partir de estos archivos se generará la imagen de firmware, copiando todos excepto los que estén dentro el direcotorio `/etc/config`, más concretamente:

* /etc/config/network
* /etc/config/babeld

Si se ingresa [nombre nodo] se modifica `/etc/config/system` para modificar el hostname.

El script modificará estos archivos según los parámetros ingresados.

Estas configuraciones deben estar basadas en lo descrito en [https://wiki.lapaz.laotrared.net/guias/configuracion_nodo_normal](https://wiki.lapaz.laotrared.net/guias/configuracion_nodo_normal)

#### TODO ####

* Probar el script en mas modelos de routers.
* Revisar los paquetes necesarios y ver si agregarlos como parametros.
* Tal vez crear un mecanismo de deteccion si hay otro proceso similar en ejecucio para esperar a que termine.
