#!/bin/bash

# Script generador de imagenes de firmware para el proyecto "LaOtraRed"
#
# Crea una imagen de firmware usando el Image Generator (ImagBuilder) de
# openwrt, lede o librecmc.
#
# Ver la funcion ayuda() para forma de uso
# 
# Autor(es):   
#            * Rodrigo Garcia <strysg@riseup.net>
#

ayuda()
{
    echo "Uso:"
    echo " ./chef.sh modelo_router bloque_IP/CIDR [bloque_Ipv6] [-t tipo]"
    echo " ./chef.sh [-l] [-h][--help]"
    echo 
    echo "Donde:"
    echo "  modelo_router    El codigo segun el ImageBuilder , del enrutador."
    echo "  bloque_IP/CIDR   El bloque de direcciones IPv4 y el /CIDR asignado al router."
    echo "  -l               Lista los codigos segun los modelos de routers."
    echo "  -h, --help       Muestra esta ayuda."
    echo "  -t tipo          Establece el tipo de archivos de configuracion a usar."
    echo "                   si no se proporciona usa las configuraciones dentro de 'deafult'"
    echo 
    echo "IMPORTANTE:"
    echo "El script requiere leer archivos PLANTILLA de configuraciones y estos deben estar ubicados en una carpeta en el directorio del script y tener el nombre 'configuraciones_chef'. "
    echo "Por cada modelo de router debe haber una carpeta nombrada con el CODIGO del MODELO del ROUTER y que contenga los archivos de configuraciones."
    echo "El script deberia estar ubicado en el directorio justo antes del  ImageBuilder que se usara por defecto, tambien funcionaria un enlace simbolico."
    echo 
    echo "Los modelos soportados estan en los archivos 'modelos_2.4Ghz.txt' y 'modelos_dualBand.txt'"
    echo 
    echo "Autor(es):   "
    echo "           * Rodrigo Garcia <strysg.riseup.net>"
    echo "Repositorio: https://git.laotrared.net/LaOtraRed-dev/generador-firmwares-script"
}

RUTA_SCRIPT=$PWD

RUTA_IMAGEBUILDER=lede-imagebuilder-ar71xx-generic.Linux-x86_64
# otros ImageBuilder
#RUTA_IMAGEBUILDER+=

DIR_CONFIGS=$PWD/configuraciones_chef
DIR_GENERADOS=$DIR_CONFIGS/generados

MODELO_ROUTER=tl-mr3020-v1
BLOQUE_IPV4='10.64.1.1/24'
MASCARA='255.255.255.0'
RED_IPV4='10.64.1.0'

BLOQUE_IPV6=''
TIPO_CONFIGS='default'

# PAQUETES
PACKAGES_TL_MR3020="kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport -wpad-mini wpad babeld"
PACKAGES_TL_WDR3600="kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport -wpad-mini wpad babeld"
#PACKAGES_TL_MR3020=kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport babeld

lista_routers()
{
    P=$PWD
    cd $RUTA_IMAGEBUILDER
    make info
    cd $P
}

if [ "$1" == "-l" ]
then
    lista_routers
    exit 1
fi

if [ -z "$1" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ -z "$2" ] 
then
    ayuda
    exit 1
fi

if [ "$3" == "-t" ]
then
    TIPO_CONFIGS=$4
    if [ ! -e $DIR_CONFIGS ]
    then
	echo "No se ha encontrado $DIR_CONFIGS"
	exit 1
    fi
fi

if [ "$4" == "-t" ]
then
    TIPO_CONFIGS=$5
    if [ ! -e $DIR_CONFIGS ]
    then
	echo "No se ha encontrado $DIR_CONFIGS"
	exit 1
    fi

fi

### preparacion 
# modelo
if [ ! -e $DIR_CONFIGS/$1 ]
then
    echo "Modelo: $1 ,no encontrado o configuraciones inexistentes"
    echo "use la opcion '-l' para ver los modelos soportados."
    exit 1
else
    MODELO_ROUTER=$1
    DIR_CONFIGS=$DIR_CONFIGS/$MODELO_ROUTER/$TIPO_CONFIGS
    echo "     Modelo DEFINIDO: $MODELO_ROUTER"
fi

# bloque IPv4
if ! echo $2 | grep -e "\/2[4|5|6|7|8|9]" &> /dev/null
then
    echo "No se ha definido CIDR valido"
    exit 1
fi

if ipcalc $2 | grep "INVALID" &> /dev/null
then
    echo "Bloque IPv4: $2, Invalido"
    exit 1
else
    BLOQUE_IPV4=$2
    MASCARA=$(ipcalc $2 | grep "Netmask" | awk '{ print $2 }')
    RED_IPV4=$(ipcalc $2 | grep "Network" | awk '{ print $2 }' | cut -d '/' -f1 )
    echo "Bloque IPv4 DEFINIDO: $2"
    echo "             MASCARA: $MASCARA"
    echo "             DIR RED: $RED_IPV4"
    echo "  Tipo configuracion: $TIPO_CONFIGS"
fi

# generar archivos de configuracion a partir de plantillas
AUX=$(echo $BLOQUE_IPV4 | cut -d '/' -f1)
AUX=$(echo $AUX | cut -d '.' -f4)
let "AUX += 1"
IPV4_ROUTER=$(echo $BLOQUE_IPV4 | cut --complement -d '.' -f4)
IPV4_ROUTER+=".$AUX"
echo "     IPv4 del router: $IPV4_ROUTER"

CIDR=$(echo $BLOQUE_IPV4 | cut -d '/' -f2)
echo "                CIDR: $CIDR"

DIR_GENERADOS=$DIR_GENERADOS/$MODELO_ROUTER"_"$RED_IPV4
# copiando archivos de las plantillas
if [ ! -e $DIR_GENERADOS ]
then
    mkdir $DIR_GENERADOS
fi

echo "  directorio destino: $DIR_GENERADOS"

rm -rf $DIR_GENERADOS &> /dev/null
cp -r $DIR_CONFIGS $DIR_GENERADOS/ &> /dev/null

## routers 2.4Ghz
if grep $MODELO_ROUTER modelos_2.4Ghz.txt
then
    ## /etc/config/network
    # IPv4
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+' *# IP publica del router 2.4Ghz"/"'$IPV4_ROUTER'       # IP publica del router 2.4Ghz"/g $DIR_CONFIGS/etc/config/network)
    echo "$AUX" > $DIR_GENERADOS/etc/config/network
    
    # CIDR
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+' *# CIDR 2.4Ghz"/"'$MASCARA'       # CIDR 2.4Ghz \/$CIDR"/g $DIR_GENERADOS/etc/config/network)
    echo "$AUX" > $DIR_GENERADOS/etc/config/network

    # IPV6n
    # ..
    
    ## /etc/config/wireless
    # (ningun cambio por ahora)

    ## /etc/config/babeld
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+\/2[4-9]' *# bloque IPv4"/"'$RED_IPV4\/$CIDR'       # bloque IPv4"/g $DIR_CONFIGS/etc/config/babeld)
    echo "$AUX" > $DIR_GENERADOS/etc/config/babeld

    ## scripts adicionales (ajustes de inicio)
    # ..

    echo 
    echo "---- etc/config/network (generado) ----"
    cat $DIR_GENERADOS/etc/config/network
    echo 
    echo "---- etc/config/babeld (generado) ----"
    cat $DIR_GENERADOS/etc/config/babeld
    echo

    # generar la imagen de firmware
    cd $RUTA_IMAGEBUILDER

    echo " ---- generando firmware -----"
    echo "make image PROFILE=$MODELO_ROUTER FILES=$DIR_GENERADOS/ PACKAGES='kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport -wpad-mini wpad babeld'"
    echo 

    # make image PROFILE=MODELO_ROUTER FILES=$DIR_GENERADOS PACKAGES=..
    if ! make image PROFILE=$MODELO_ROUTER FILES=$DIR_GENERADOS/ PACKAGES="kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport -wpad-mini wpad babeld" > /dev/null
    then
	echo "ERROR al generar el firmware"
	exit 1
    fi

    # copiando imagen generada
    cd $RUTA_SCRIPT
    cd $RUTA_IMAGEBUILDER

    IMAGEN_GENERADA=$(find . -name *$MODELO_ROUTER-squashfs-factory.bin -type f | head -n 1)
    echo "IMG: $IMAGEN_GENERADA"
    cp $IMAGEN_GENERADA $DIR_GENERADOS

    # creando comprimido de la carpeta de configuraciones
    cd $DIR_GENERADOS
    echo "comprimiendo carpeta de configuraciones"
    zip -r configuraciones etc > /dev/null

    ### mostrar reportes
    echo
    echo "IMAGEN GENERADA en:    $DIR_GENERADOS"

    exit 0
fi

## routers DUAL BAND
if grep $MODELO_ROUTER modelos_dualBand.txt
then
    # /etc/config/network
    # IPv4 2.4Ghz
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+' *# IP publica del router 2.4Ghz"/"'$IPV4_ROUTER'       # IP publica del router 2.4Ghz"/g $DIR_CONFIGS/etc/config/network)
    echo "$AUX" > $DIR_GENERADOS/etc/config/network
    
    # CIDR 2.4Ghz
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+' *# CIDR 2.4Ghz"/"'$MASCARA'       # CIDR 2.4Ghz \/$CIDR"/g $DIR_GENERADOS/etc/config/network)
    echo "$AUX" > $DIR_GENERADOS/etc/config/network

    # IPv4 5Ghz
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+' *# IP publica del router 5Ghz"/"'$IPV4_ROUTER'       # IP publica del router 5Ghz"/g $DIR_GENERADOS/etc/config/network)
    echo "$AUX" > $DIR_GENERADOS/etc/config/network
    
    # CIDR 5Ghz
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+' *# CIDR 5Ghz"/"'$MASCARA'       # CIDR 5Ghz \/$CIDR"/g $DIR_GENERADOS/etc/config/network)
    echo "$AUX" > $DIR_GENERADOS/etc/config/network

    ## /etc/config/babeld
    AUX=$(sed --regexp-extended s/"'[0-9]+.[0-9]+.[0-9]+.[0-9]+\/2[4-9]' *# bloque IPv4"/"'$RED_IPV4\/$CIDR'       # bloque IPv4"/g $DIR_CONFIGS/etc/config/babeld)
    echo "$AUX" > $DIR_GENERADOS/etc/config/babeld

    echo 
    echo "---- etc/config/network (generado) ----"
    cat $DIR_GENERADOS/etc/config/network
    echo 
    echo "---- etc/config/babeld (generado) ----"
    cat $DIR_GENERADOS/etc/config/babeld
    echo

    # generar la imagen de firmware
    cd $RUTA_SCRIPT
    cd $RUTA_IMAGEBUILDER

    echo " ---- generando firmware -----"
    echo "make image PROFILE=$MODELO_ROUTER FILES=$DIR_GENERADOS/ PACKAGES='kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport -wpad-mini wpad babeld'"
    echo 

    # make image PROFILE=MODELO_ROUTER FILES=$DIR_GENERADOS PACKAGES=..
    if ! make image PROFILE=$MODELO_ROUTER FILES=$DIR_GENERADOS PACKAGES="kmod-usb-core kmod-usb2 kmod-usb-ledtrig-usbport -wpad-mini wpad babeld" > /dev/null
    then
	echo "ERROR al generar el firmware"
	exit 1
    fi
    # copiando imagen generada
    cd $RUTA_IMAGEBUILDER

    IMAGEN_GENERADA=$(find . -name *$MODELO_ROUTER-squashfs-factory.bin -type f | head -n 1)
    echo "IMG: $IMAGEN_GENERADA"
    
    cp $IMAGEN_GENERADA $DIR_GENERADOS

    # creando comprimido de la carpeta de configuraciones
    cd $DIR_GENERADOS
    echo "comprimiendo carpeta de configuraciones"
    zip -r configuraciones etc > /dev/null

    ### mostrar reportes
    echo
    echo "IMAGEN GENERADA en:     $DIR_GENERADOS"

    exit 0
fi

exit 1
