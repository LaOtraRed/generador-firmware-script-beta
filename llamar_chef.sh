#!/bin/bash

# script para llamar al chef.sh desde aplicaciones o subprocesos
# y que no se generen problemas en la ruta actual

# al llamar a este script se debe pasarle la ruta absoluta
# de la ubicacion del chef.sh
# ademas de los argumentos

# ejemplo:
# ./llamar-chef '/ruta/donde/esta/el/chef.sh' modelo_router bloque_IP/CIDR

RUTA_CHEF=$1

MODELO_ROUTER=$2
BLOQUE_IPV4_CIDR=$3

#TODO: arreglar la opcion -t
OP_CONFIG=$4
TIPO_CONFIG=$5

# ejecutar
cd $RUTA_CHEF

if ! ./chef.sh $MODELO_ROUTER $BLOQUE_IPV4_CIDR $OP_CONFIG $TIPO_CONFIG
then
    exit 1
fi

exit 0




